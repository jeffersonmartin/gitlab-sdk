<?php

namespace GitlabIt\Gitlab\Exceptions;

use Exception;

class ForbiddenException extends Exception
{
    //
}
