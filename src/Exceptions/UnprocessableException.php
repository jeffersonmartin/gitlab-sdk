<?php

namespace GitlabIt\Gitlab\Exceptions;

use Exception;

class UnprocessableException extends Exception
{
    //
}
