<?php

namespace GitlabIt\Gitlab\Exceptions;

use Exception;

class PreconditionFailedException extends Exception
{
    //
}
