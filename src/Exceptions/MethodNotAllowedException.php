<?php

namespace GitlabIt\Gitlab\Exceptions;

use Exception;

class MethodNotAllowedException extends Exception
{
    //
}
