<?php

namespace GitlabIt\Gitlab\Exceptions;

use Exception;

class NotFoundException extends Exception
{
    //
}
